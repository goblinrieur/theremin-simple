# theremin simple

Theremin simple à 2 antennes sous [LICENSE](./LICENSE) Open Source Creative Commons.

L'idée ici est de fabriquer un theremin simple, sans besoin de qualité particulière juste un "jouet" d'apprentissage.

# concept

[principe général](https://commons.wikimedia.org/wiki/File:Block_diagram_Theremin.png)

_source wikipedia_

On va simplifier ici en remplaçant toute la partie volume par un simple potentiomètre. 

Ainsi, une seule antenne ne soit nécessaire et on ne contrôlera donc que la fréquence sonore à proximité de l'antenne.

J'ai en stock un TLC272 ou un MCP602 qui servira donc d'AOP entre le réglage du volume et le "signal" fréquentiel.

L'amplification audio de sortie sera un simple LM386 pour un Haut-parleur 8ohm standard. _(gain faible ici, il n'est pas utile de faire plus)_

Pour simplifier un NAND a trigger sera utilisé, sûrement un 4093 par ce que j'ai en stock et qu'en alimentation sur pile ou batterie Lipo permet de s'affranchir de toute alimentation complexe.

Comme ceci le schema restera simple et facile à comprendre pour un néophyte.

# images

![schema](./Schema.png)

![copper](./thrmn/thrmn_copper.png)

![FACE](./thrmn/thrmn.png)

